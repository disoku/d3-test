import * as d3 from 'd3'

export default (function () {
    const dispatch = d3.dispatch('sliderchange');

    const sourceData = {
        numberOfDataSet: 1,
        flights: [],
        airports: [],
    };

    const needReloadData = () => !sourceData.airports.length || !sourceData.flights.length;

    const getAirports = async () => {
        const response = await fetch('data/airports.csv');
        if (response.ok) {
            return await response.text();
        }
        throw new Error(response.status)
    };

      const  getFlights = async (number = 1) => {
        const response = await fetch(`data/dataset-${number}.csv`);
        if (response.ok) {
            return await response.text();
        }
        throw new Error(response.status)
    };

    const loadSourceData = async () => {
        const data = await Promise.all([getAirports(), getFlights()]);

        sourceData.airports = d3.csvParse(data[0]);
        sourceData.flights = d3.csvParse(data[1]);
    };

    return {
        call: (type, that, args) => {
            dispatch.call(type, that, args)
        },

        on: (type, that, args) => {
            dispatch.on(type, that, args)
        },

        getUSA: async () => {
            const response = await fetch('data/us.json');
            if (response.ok) {
                return await response.json();
            }
            throw new Error(response.status)
        },

        filterFlights: async (startDate, endDate) => {
            if (needReloadData()) {
                await loadSourceData();
            }
            return sourceData.flights
                .reduce((acc, flight) => {
                    const date = new Date(+new Date(+flight.Year, +flight.Month + 1, +flight.DayofMonth) + flight.CRSDepTime * 60 * 1000);
                    if (date - startDate >= 0 && endDate - date >= 0 || !startDate || !endDate) {
                        return acc.concat({
                            ...flight,
                            date
                        });
                    }
                    return acc;
                }, [])
                .sort((a, b) => a.date - b.date)
        },


        filterAirPorts: async (startDate, endDate) => {
            if (needReloadData()) {
                await loadSourceData();
            }

            const hashTable = Object.create(null);

            sourceData.flights.filter(flight => {
                const date = new Date(+new Date(+flight.Year, +flight.Month + 1, +flight.DayofMonth) + flight.CRSDepTime * 60 * 1000);
                const isFlightSuitable = date - startDate >= 0 && endDate - date >= 0 || !startDate || !endDate;
                if (isFlightSuitable) {
                    hashTable[flight.Origin] = true;
                    hashTable[flight.Dest] = true;
                }

                hashTable['SJU'] = false;
                return isFlightSuitable;
            });

            return sourceData.airports.filter(airport => !!hashTable[airport.iata]);
        }
    }
})()

