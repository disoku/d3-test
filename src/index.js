import * as d3 from 'd3'
import './style.css';
import Slider from './charts/Slider';
import BarLineChart from './charts/BarLineChart';
import MapChart from './charts/MapChart';
import dataService from './services/dataService'

let startDate, endDate;

const app = async () => {
    d3.select('#slider')
        .call(Slider());

    const airports = await dataService.filterAirPorts(startDate, endDate);
    const flights = await dataService.filterFlights(startDate, endDate);
    const mapChart = MapChart()
        .load({airports, flights});

    d3.select('#map')
        .call(mapChart);

    const barLineChart = BarLineChart()
        .load({flights});
    d3.select('#bar-line')
        .call(barLineChart);

    dataService.on('sliderchange.map', async d => {
        startDate = d.min;
        endDate = d.max;
        const airports = await dataService.filterAirPorts(startDate, endDate);
        const flights = await dataService.filterFlights(startDate, endDate);
        mapChart.update({airports, flights});
        barLineChart.update({flights});
    });

};

app();