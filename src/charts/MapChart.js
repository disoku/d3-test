import * as d3 from 'd3';
import * as topojson from 'topojson';
import dataService from '../services/dataService'
import './map.css';

export default function () {
    let initialData, allAirportsWithCoords;

    let width = 960,
        height = 500,
        margin = { top: 0, right: 10, bottom: 0, left: 10 },
        svg,
        div,
        updateData,
        applyProjection,
        mouseoverLegend,
        mouseoutLegend,
        showFlights;

    function chart(selection) {
        selection.each(async function (d) {
            div = d3.select('body').append('div')
                .attr('class', 'tooltip')
                .style('opacity', 0);
            svg = selection.append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom);

            document.getElementsByClassName('show-flights-checkbox')[0].addEventListener(
                'change',
                function () {
                    showFlights = this.checked;
                    updateData();
                },
                false
            );

            const projection = d3.geoAlbersUsa();
            const path = d3.geoPath().projection(projection);

            try {
                const data = await dataService.getUSA();
                const airports = applyProjection(initialData.airports, projection);
                createAirportsMap(airports);
                const states = topojson.feature(data, data.objects.states);

                const countries = svg.selectAll('path.state')
                    .data(states.features).enter();

                countries.insert('path', '.graticule')
                    .attr('class', d => `state-${d.id}`)
                    .style('fill', '#ccc')
                    .attr('d', path)
                    .on('mouseover', mouseoverLegend).on('mouseout', mouseoutLegend)

                countries
                    .append('svg:text')
                    .attr('class', d => `state-${d.id}-label info`)
                    .attr('transform', d => `translate(${width - 6 * d.properties.name.length}, 15)`)
                    .attr('dy', '.35em')
                    .append('svg:tspan')
                    .attr('x', 0)
                    .attr('dy', 5)
                    .text(d => d.properties.name);

                svg.selectAll('circle')
                    .data(airports).enter()
                    .append('circle')
                    .attr('cx', d => d.x)
                    .attr('cy', d => d.y)
                    .attr('r', '1px')
                    .attr('fill', 'red');
            } catch (err) {
                console.error('Error in MapChart: ' + err);
            }

            updateData = () => {
                const airports = applyProjection(initialData.airports, projection);
                const airportsSelection = svg
                    .selectAll('circle')
                    .data(airports);
                airportsSelection.exit().remove();
                airportsSelection.enter()
                    .append('circle')
                    .merge(airportsSelection)
                    .attr('cx', d => d.x)
                    .attr('cy', d => d.y)
                    .attr('r', '1px')
                    .attr('fill', 'red');

                if (showFlights) {
                    const flightsSelection = svg
                        .selectAll('line')
                        .data(getFlights());
                    flightsSelection.exit().remove();
                    flightsSelection.enter()
                        .append('line')
                        .merge(flightsSelection)
                        .attr('x1', d => allAirportsWithCoords.get(d.Origin).x)
                        .attr('y1', d => allAirportsWithCoords.get(d.Origin).y)
                        .attr('x2', d => allAirportsWithCoords.get(d.Dest).x)
                        .attr('y2', d => allAirportsWithCoords.get(d.Dest).y)
                        .style('stroke', 'black')
                        .style('stroke-width', '0.2')
                        .style('stroke-opacity', 0)
                        .transition()
                        .duration(555)
                        .style('stroke-opacity', 1)
                } else {
                    svg
                        .selectAll('line')
                        .transition()
                        .duration(555)
                        .style('stroke-opacity', 0)
                        .remove();
                }

            };

            function getFlights() {
                return initialData.flights.reduce((acc, flight) => {
                    if (allAirportsWithCoords.get(flight.Origin) && allAirportsWithCoords.get(flight.Dest)) {
                        return acc.concat(flight);
                    }
                    return acc;
                }, [])
            }
        });
    }

    chart.width = function (value) {
        if (!arguments.length) return width;
        width = value;
        return chart;
    };

    chart.height = function (value) {
        if (!arguments.length) return height;
        height = value;
        return chart;
    };

    chart.update = (data) => {
        if (!data) {
            return initialData;
        }
        initialData = data;
        updateData();

        return chart;
    };

    chart.load = (data) => {
        if (!data) {
            return initialData;
        }
        initialData = data;
        return chart;
    };

    applyProjection = (airports, projection) => {
        return airports.reduce((acc, airport) => {
            const proj = projection([+airport.long, +airport.lat]);
            if (proj) {
                return acc.concat({ x: proj[0], y: proj[1], iata: airport.iata });
            }
            return acc;
        }, []);
    };

    mouseoverLegend = (d) => {
        d3.selectAll(`.state-${d.id}`)
            .style('fill', '#cc6699');

        d3.selectAll(`.state-${d.id}-label`)
            .style('display', 'inline-block');

        div.transition()
            .duration(200)
            .style('opacity', .9);

        div.html(d.properties.name)
            .style('left', (d3.event.pageX) + 'px')
            .style('top', (d3.event.pageY - 28) + 'px')
            .style('width', 8 * d.properties.name.length + 'px');
    };

    mouseoutLegend = (d) => {
        d3.selectAll(`.state-${d.id}`)
            .style('fill', '#ccc');

        d3.selectAll(`.state-${d.id}-label`)
            .style('display', 'none');

        div.transition()
            .duration(500)
            .style('opacity', 0);
    };

    function createAirportsMap(airports) {
        allAirportsWithCoords = airports.reduce((acc, airport) => {
            acc.set(airport.iata, {
                x: airport.x,
                y: airport.y
            });
            return acc;
        }, new Map());
    }

    return chart;
}




