import * as d3 from 'd3';
import './slider.css';
import dataService from '../services/dataService';

export default function () {
    let width = 1400, height = 50,
        margin = { top: 0, right: 10, bottom: 0, left: 10 * 10 },
        svg, handle, handle2, min = 10, max = 100;

    // 2. c.  Slider shouldn't depend on an external dispatch, implement the dispatch object in the slider.
    // Before You Start j. Create an independent service, that is responsible: ii. For updating the representation of charts depending on changes to sliders, use d3.dispatch here.
    // ??
    const dispatch = d3.dispatch('dragend');

    dispatch.on('dragend', function (data) {
        dataService.call('sliderchange', this, data);
    });

    function chart(selection) {
        selection.each(function (d) {
            let start = min = new Date(2007, 0, 1), end = max = new Date(2008, 0, 1);

            svg = selection.append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom);

            const x = d3.scaleTime()
                .domain([start, end]) // change to our goals
                .range([0, width - margin.left - margin.right])
                .clamp(true);

            const slider = svg.append('g')
                .attr('class', 'slider')
                .attr('transform', `translate(${margin.left},${height / 2})`);

            slider.append('line')
                .attr('class', 'track')
                .attr('x1', x.range()[0])
                .attr('x2', x.range()[1])
                .select(function () {
                    return this.parentNode.appendChild(this.cloneNode(true));
                })
                .attr('class', 'track-inset')
                .select(function () {
                    return this.parentNode.appendChild(this.cloneNode(true));
                })
                .attr('class', 'track-overlay')
                .call(
                    d3.drag()
                        .on('start.interrupt', () => slider.interrupt())
                        .on('start drag', () => hue(x.invert(d3.event.x)))
                        .on('end', function () {
                            dispatch.call('dragend', this, {min, max});
                        }));

            const timeFormat = d3.timeFormat("%d-%b-%y");
            slider.insert('g', '.track-overlay')
                .attr('class', 'ticks')
                .attr('transform', `translate(0, 18)`)
                .selectAll('text')
                .data(x.ticks())
                .enter().append('text')
                .attr('x', x)
                .attr('text-anchor', 'middle')
                .text(d => timeFormat(d));

            handle = slider.insert('circle', '.track-overlay')
                .attr('class', 'handle')
                .attr('r', 9);
            handle2 = slider.insert('circle', '.track-overlay')
                .attr('class', 'handle2')
                .attr('r', 9);

            slider.transition()
                .duration(750)
                .tween('hue', () => {
                    const i = d3.interpolate(0, width - margin.left - margin.right);
                    return t => hue(x.invert(i(t)));
                });

            dispatch.on('dragend.display-selected-values', data => {
                const selectedValues = svg
                    .selectAll('text.selected-values')
                    .data([data.min, data.max]);
                selectedValues.exit().remove();
                selectedValues.enter()
                    .append('text')
                    .merge(selectedValues)
                    .attr('class', 'selected-values')
                    .attr('text-anchor', 'middle')
                    .attr('fill', 'red')
                    .attr('x', (d, i) => i ? width + 50 : 40)
                    .attr('y', 30)
                    .text(d => timeFormat(d));
            });

            function hue(h) {
                if (Math.abs(h - min) < Math.abs(h - max)) {
                    handle.attr('cx', x(h));
                    min = h;
                } else {
                    handle2.attr('cx', x(h));
                    max = h;
                }
            }
        })
    }

    chart.width = function (value) {
        if (!arguments.length) return width;
        width = value;
        return chart;
    };

    chart.height = function (value) {
        if (!arguments.length) return height;
        height = value;
        return chart;
    };

    return chart;
};




