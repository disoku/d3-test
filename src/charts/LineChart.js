import * as d3 from 'd3';
import './line.css';

export default function () {
    let width = 1100, height = 500, updateData,
        margin = { top: 0, right: 10, bottom: 40, left: 25 },
        data, x, y, toggleSeries, dispatch;

    const delayTypes = ['CarrierDelay', 'WeatherDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay'],
        activeSeries = new Set(['CarrierDelay', 'WeatherDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay']),
        delayTypesColors = ['red', 'green', 'black', 'blue', '#808000'];



    function chart(selection) {
        selection.each(function (d) {
            const container = d3.select(this)
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .classed('line-container', true)
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            const linesData = getLinesData();

            x = d3.scaleTime()
                .rangeRound([0, width]);
            x.domain(d3.extent(data.flights, d => d.date));

            const seriesContainer = container
                .append('g')
                .attr('class', 'series-container')
                .attr('transform', `translate(${width + 50}, 50)`);

            seriesContainer.selectAll('text')
                .data(delayTypes)
                .enter()
                .append('text')
                .attr('class', d => `${d}-line`)
                .attr('x', d => d.cx)
                .attr('y', (d, i) => 12 * i)
                .text(d => d)
                .attr('font-family', 'sans-serif')
                .attr('font-size', '12px')
                .attr('fill', (d, i) => delayTypesColors[i])
                .on('click', (d, i) => toggleSeries(d));

            y = d3.scaleLinear()
                .rangeRound([height, 0]);
            y.domain(d3.extent(data.flights, d => d3.max(delayTypes.map(type => +d[type]))));

            const line = d3.line()
                .curve(d3.curveBasis)
                .x(d => x(d.date));

            const lines = container
                .selectAll('path')
                .data(linesData)
                .enter().append('path')
                .attr('class', d => `line-${d.type} line`)
                .attr('fill', 'none')
                .attr('stroke', (d, i) => delayTypesColors[i])
                .attr('stroke-linejoin', 'round')
                .attr('stroke-linecap', 'round')
                .attr('stroke-width', 0.6)
                .attr('d', d => {
                    line.y(d2 => y(+d2[d.type]));
                    return line(data.flights)
                });
            lines
                .on('mouseover', activateBar)
                .on('mouseout', deactivateBar);

            updateData = () => {
                x.domain(d3.extent(data.flights, d => d.date));
                y.domain(d3.extent(data.flights, d => d3.max(delayTypes.map(type => +d[type]))));

                const linesData = getLinesData();

                const linesSelection = container
                    .selectAll('path.line')
                    .data(linesData);

                linesSelection.exit().remove();
                linesSelection.enter()
                    .append('path')
                    .merge(linesSelection)
                    .attr('class', d => `line-${d.type} line`)
                    .attr('fill', 'none')
                    .attr('stroke', (d, i) => delayTypesColors[i])
                    .attr('stroke-linejoin', 'round')
                    .attr('stroke-linecap', 'round')
                    .attr('stroke-width', 0.6)
                    .attr('stroke-opacity', d => activeSeries.has(d.type) ? 1 : 0)
                    .attr('d', d => {
                        line.y(d2 => y(+d2[d.type]));
                        return line(data.flights)
                    });
                lines
                    .on('mouseover', activateBar)
                    .on('mouseout', deactivateBar);
            };

            toggleSeries = d => {
                activeSeries.has(d) ? activeSeries.delete(d) : activeSeries.add(d);
                container.select(`.line-${d}`)
                    .transition()
                    .duration(750)
                    .attr('stroke-opacity', activeSeries.has(d) ? 1 : 0);

                seriesContainer
                    .select(`.${d}-line`)
                    .attr('text-decoration', activeSeries.has(d) ? 'none' : 'line-through');
            };

            function activateBar(d) {
                d3.select(this).classed('active', true);
                dispatch.call('show-tooltip', this, {d, show: true});
            }

            function deactivateBar() {
                d3.select(this).classed('active', false);
                dispatch.call('show-tooltip', this, {d, show: false});
            }

            function getLinesData() {
                const linesData = [];
                delayTypes.forEach(type => {
                    const lineData = data.flights.reduce((acc, flight) => {
                        acc.type = type;
                        acc.value = acc.value ?acc.value + +flight[type] : +flight[type];
                        return acc;
                    }, {});
                    linesData.push(lineData);
                });
                return linesData;
            }
        })
    }


    chart.width = function (value) {
        if (!arguments.length) return width;
        width = value;
        return chart;
    };

    chart.height = function (value) {
        if (!arguments.length) return height;
        height = value;
        return chart;
    };

    chart.yScale = function (value) {
        if (!arguments.length) return y;
        y = value;
        return chart;
    };

    chart.xScale = function (scale) {
        if (!arguments.length) return x;
        x = scale;
        return chart;
    };

    chart.update = (d) => {
        if (!d) {
            return data;
        }
        data = d;
        updateData();
        return chart;
    };

    chart.load = (d) => {
        if (!d) {
            return data;
        }
        data = d;
        return chart;
    };

    chart.dispatch = (d) => {
        if (!d) {
            return dispatch;
        }
        dispatch = d;
        return chart;
    };

    return chart;
};




