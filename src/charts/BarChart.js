import * as d3 from 'd3';
import './bar.css';

export default function () {
    let width = 1100, height = 500, updateData,
        margin = { top: 0, right: 10, bottom: 30, left: 25 },
        data, x, y, dispatch;

    function chart(selection) {
        selection.each(function (d) {

            const container = d3.select(this)
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .classed('bar-container', true)
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            x = d3.scaleBand()
                .range([0, width])
                .padding(0.1);
            y = d3.scaleLinear()
                .range([height, 0]);

            x.domain(data.flights.map(d => d.date));
            y.domain([0, d3.max(data.flights,  d => +d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay)]);

            container.selectAll('.bar')
                .data(data.flights)
                .enter().append('rect')
                .on('mouseover', activateBar)
                .on('mouseout', deactivateBar)
                .attr('class', 'bar')
                .attr('x', d => x(d.date))
                .attr('width', x.bandwidth())
                .attr('y', d => y(+d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay))
                .attr('height', d => height - y(+d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay));

            updateData = () => {
                const barsSelection = container
                    .selectAll('rect.bar')
                    .data(data.flights);

                x.domain(data.flights.map(d => d.date));
                y.domain([0, d3.max(data.flights, d => +d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay)]);

                barsSelection.exit().remove();
                barsSelection.enter()
                    .append('rect')
                    .merge(barsSelection)
                    .on('mouseover', activateBar)
                    .on('mouseout', deactivateBar)
                    .attr('class', 'bar')
                    .attr('x', d => x(d.date))
                    .attr('width', x.bandwidth())
                    .attr('y', height)
                    .attr('height', 0)
                    .transition()
                    .duration(350)
                    .attr('y', d => y(+d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay))
                    .attr('height', d => height - y(+d.CarrierDelay + +d.WeatherDelay + +d.NASDelay + +d.SecurityDelay + +d.LateAircraftDelay));

            };

            function activateBar(d) {
                d3.select(this).classed('active', true);
                dispatch.call('show-tooltip', this, {d, show: true});
            }

            function deactivateBar() {
                d3.select(this).classed('active', false);
                dispatch.call('show-tooltip', this, {d, show: false});
            }
        })
    }

    chart.width = function (value) {
        if (!arguments.length) return width;
        width = value;
        return chart;
    };

    chart.height = function (value) {
        if (!arguments.length) return height;
        height = value;
        return chart;
    };

    chart.yScale = function (value) {
        if (!arguments.length) return y;
        y = value;
        return chart;
    };

    chart.xScale = function (scale) {
        if (!arguments.length) return x;
        x = scale;
        return chart;
    };

    chart.update = (d) => {
        if (!d) {
            return data;
        }
        data = d;
        updateData();
        return chart;
    };

    chart.load = (d) => {
        if (!d) {
            return data;
        }
        data = d;
        return chart;
    };

    chart.dispatch = (d) => {
        if (!d) {
            return dispatch;
        }
        dispatch = d;
        return chart;
    };

    return chart;
};




