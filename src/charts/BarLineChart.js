import * as d3 from 'd3';
import BarChart from './BarChart';
import LineChart from './LineChart';

export default function () {
    let width = 1300, height = 500, initialData, updateData, x,
        margin = { top: 20, right: 10, bottom: 30, left: 110 },
        svg;

    const dispatch = d3.dispatch('show-tooltip');

    function chart(selection) {
        selection.each(function (d) {
            const barChart = BarChart()
                    .dispatch(dispatch)
                    .load(initialData),
                lineChart = LineChart()
                    .dispatch(dispatch)
                    .load(initialData);

            const timeFormat = d3.timeFormat('%d-%b-%y');

            const container = selection.append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .classed('bar-line-container', true);

            // Define the div for the tooltip
            const tooltip = d3.select('body').append('div')
                .attr('class', 'bar-line-tooltip')
                .style('opacity', 0);

            container
                .call(barChart)
                .call(lineChart);

            dispatch.on('show-tooltip', function(d) {
                if (d.show) {
                    const content = d.d.type
                        ? `${d.d.type} ` + '<br/>' + `Date ${d.d.value}`
                        : `Delays ${+d.d.CarrierDelay + +d.d.WeatherDelay + +d.d.NASDelay + +d.d.SecurityDelay + +d.d.LateAircraftDelay} ` + '<br/>' + `${timeFormat(d.d.date)}`;

                    tooltip.transition()
                        .duration(200)
                        .style('opacity', .9);
                    tooltip.html(content)
                        .style('left', (d3.event.pageX) + 'px')
                        .style('top', (d3.event.pageY - 28) + 'px');
                } else {
                    tooltip.transition()
                        .duration(500)
                        .style('opacity', 0);
                }
            });

            x = d3.scaleTime()
                .rangeRound([0, lineChart.width()])
                .domain(d3.extent(initialData.flights, d => d.date));

            container.append('g')
                .attr('class', 'x-axis')
                .attr('transform', `translate(25, ${height})`)
                .call(d3.axisBottom(x));

            const leftY = barChart.yScale(),
                rightY = lineChart.yScale();

            container.append('g')
                .attr('class', 'y-axis-left')
                .attr('transform', `translate(25, 0)`)
                .call(d3.axisLeft(leftY));

            container.append('g')
                .attr('class', 'y-axis-right')
                .attr('transform', `translate(1125, 0)`)
                .call(d3.axisRight(rightY));

            updateData = () => {
                barChart.update(initialData);
                lineChart.update(initialData);

                x.domain(d3.extent(initialData.flights, d => d.date));

                container.select('.x-axis')
                    .transition()
                    .duration(750)
                    .call(d3.axisBottom(x));

                container.select('.y-axis-left')
                    .transition()
                    .duration(750)
                    .call(d3.axisLeft(barChart.yScale()));

                container.select('.y-axis-right')
                    .transition()
                    .duration(750)
                    .call(d3.axisRight(lineChart.yScale()));
            };
        })
    }

    chart.width = function (value) {
        if (!arguments.length) return width;
        width = value;
        return chart;
    };

    chart.height = function (value) {
        if (!arguments.length) return height;
        height = value;
        return chart;
    };

    chart.update = (data) => {
        if (!data) {
            return initialData;
        }
        initialData = data;
        updateData();

        return chart;
    };

    chart.load = (data) => {
        if (!data) {
            return initialData;
        }
        initialData = data;
        return chart;
    };

    return chart;
};




